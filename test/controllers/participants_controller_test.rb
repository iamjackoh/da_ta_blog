require 'test_helper'

class ParticipantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @participant = participants(:one)
  end

  test "should get index" do
    get participants_url
    assert_response :success
  end

  test "should get new" do
    get new_participant_url
    assert_response :success
  end

  test "should create participant" do
    assert_difference('Participant.count') do
      post participants_url, params: { participant: { amount_of_garbage: @participant.amount_of_garbage, duration_of_stay: @participant.duration_of_stay, if_they_recycle: @participant.if_they_recycle, if_they_recycle_in_IGC: @participant.if_they_recycle_in_IGC, location: @participant.location, recycling_knowledge1: @participant.recycling_knowledge1, recycling_knowledge2: @participant.recycling_knowledge2 } }
    end

    assert_redirected_to participant_url(Participant.last)
  end

  test "should show participant" do
    get participant_url(@participant)
    assert_response :success
  end

  test "should get edit" do
    get edit_participant_url(@participant)
    assert_response :success
  end

  test "should update participant" do
    patch participant_url(@participant), params: { participant: { amount_of_garbage: @participant.amount_of_garbage, duration_of_stay: @participant.duration_of_stay, if_they_recycle: @participant.if_they_recycle, if_they_recycle_in_IGC: @participant.if_they_recycle_in_IGC, location: @participant.location, recycling_knowledge1: @participant.recycling_knowledge1, recycling_knowledge2: @participant.recycling_knowledge2 } }
    assert_redirected_to participant_url(@participant)
  end

  test "should destroy participant" do
    assert_difference('Participant.count', -1) do
      delete participant_url(@participant)
    end

    assert_redirected_to participants_url
  end
end
