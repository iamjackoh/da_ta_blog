require 'test_helper'

class NonRecyclersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @non_recycler = non_recyclers(:one)
  end

  test "should get index" do
    get non_recyclers_url
    assert_response :success
  end

  test "should get new" do
    get new_non_recycler_url
    assert_response :success
  end

  test "should create non_recycler" do
    assert_difference('NonRecycler.count') do
      post non_recyclers_url, params: { non_recycler: { improvements: @non_recycler.improvements, participants_id: @non_recycler.participants_id, suggestion: @non_recycler.suggestion, why_no_recycle: @non_recycler.why_no_recycle } }
    end

    assert_redirected_to non_recycler_url(NonRecycler.last)
  end

  test "should show non_recycler" do
    get non_recycler_url(@non_recycler)
    assert_response :success
  end

  test "should get edit" do
    get edit_non_recycler_url(@non_recycler)
    assert_response :success
  end

  test "should update non_recycler" do
    patch non_recycler_url(@non_recycler), params: { non_recycler: { improvements: @non_recycler.improvements, participants_id: @non_recycler.participants_id, suggestion: @non_recycler.suggestion, why_no_recycle: @non_recycler.why_no_recycle } }
    assert_redirected_to non_recycler_url(@non_recycler)
  end

  test "should destroy non_recycler" do
    assert_difference('NonRecycler.count', -1) do
      delete non_recycler_url(@non_recycler)
    end

    assert_redirected_to non_recyclers_url
  end
end
