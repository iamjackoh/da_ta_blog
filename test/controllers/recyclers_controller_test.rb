require 'test_helper'

class RecyclersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recycler = recyclers(:one)
  end

  test "should get index" do
    get recyclers_url
    assert_response :success
  end

  test "should get new" do
    get new_recycler_url
    assert_response :success
  end

  test "should create recycler" do
    assert_difference('Recycler.count') do
      post recyclers_url, params: { recycler: { improvements: @recycler.improvements, participants_id: @recycler.participants_id, satisfaction: @recycler.satisfaction, satisfaction_why: @recycler.satisfaction_why, suggestion: @recycler.suggestion } }
    end

    assert_redirected_to recycler_url(Recycler.last)
  end

  test "should show recycler" do
    get recycler_url(@recycler)
    assert_response :success
  end

  test "should get edit" do
    get edit_recycler_url(@recycler)
    assert_response :success
  end

  test "should update recycler" do
    patch recycler_url(@recycler), params: { recycler: { improvements: @recycler.improvements, participants_id: @recycler.participants_id, satisfaction: @recycler.satisfaction, satisfaction_why: @recycler.satisfaction_why, suggestion: @recycler.suggestion } }
    assert_redirected_to recycler_url(@recycler)
  end

  test "should destroy recycler" do
    assert_difference('Recycler.count', -1) do
      delete recycler_url(@recycler)
    end

    assert_redirected_to recyclers_url
  end
end
