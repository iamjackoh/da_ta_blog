Rails.application.routes.draw do
  get 'explore/if_they_recycle'
  get 'explore/amount_of_garbage'
  get 'explore/satisfaction'
  get 'explore/table_of_contents'
  get 'explore/analysis'
  get 'team_data/load'
  get 'participants/index'
  get 'recyclers/index'
  get 'non_recyclers/index'
  
  resources :participants
  devise_for :users, controllers: {omniauth_callbacks: 'users/omniauth_callbacks' }
  resources :non_recyclers
  resources :recyclers
  resources :blogs
  
  root to: "explore#table_of_contents"
  
  
end