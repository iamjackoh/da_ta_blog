class NonRecyclersController < ApplicationController
  before_action :set_non_recycler, only: [:show, :edit, :update, :destroy]

  # GET /non_recyclers
  # GET /non_recyclers.json
  def index
    @non_recyclers = NonRecycler.all
  end

  # GET /non_recyclers/1
  # GET /non_recyclers/1.json
  def show
  end

  # GET /non_recyclers/new
  def new
    @non_recycler = NonRecycler.new
  end

  # GET /non_recyclers/1/edit
  def edit
  end

  # POST /non_recyclers
  # POST /non_recyclers.json
  def create
    @non_recycler = NonRecycler.new(non_recycler_params)

    respond_to do |format|
      if @non_recycler.save
        format.html { redirect_to @non_recycler, notice: 'Non recycler was successfully created.' }
        format.json { render :show, status: :created, location: @non_recycler }
      else
        format.html { render :new }
        format.json { render json: @non_recycler.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /non_recyclers/1
  # PATCH/PUT /non_recyclers/1.json
  def update
    respond_to do |format|
      if @non_recycler.update(non_recycler_params)
        format.html { redirect_to @non_recycler, notice: 'Non recycler was successfully updated.' }
        format.json { render :show, status: :ok, location: @non_recycler }
      else
        format.html { render :edit }
        format.json { render json: @non_recycler.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /non_recyclers/1
  # DELETE /non_recyclers/1.json
  def destroy
    @non_recycler.destroy
    respond_to do |format|
      format.html { redirect_to non_recyclers_url, notice: 'Non recycler was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_non_recycler
      @non_recycler = NonRecycler.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def non_recycler_params
      params.require(:non_recycler).permit(:why_no_recycle, :improvements, :suggestion, :participants_id)
    end
end
