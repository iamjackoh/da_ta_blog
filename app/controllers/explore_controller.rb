class ExploreController < ApplicationController
  def if_they_recycle
    @data = Participant.group( :if_they_recycle, :if_they_recycle_in_IGC ).count.to_a
  end
  def amount_of_garbage
    @data = Participant.group( :amount_of_garbage ).count.to_a
  end
  def satisfaction
    @data = Recycler.group( :satisfaction ).count.to_a
  end
end
