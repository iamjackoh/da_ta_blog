class TeamDataController < ApplicationController
  def load
	current_user.refresh_token_if_expired
	session = GoogleDrive::Session.from_access_token(current_user.token)
	
	#Get
	sheet = session.spreadsheet_by_title('IGC Waste Disposal Survey Answers').worksheet_by_title('Form Responses 1')
	
	(2..34).each do |row_num|
		locaton = sheet[row_num,2]
		duration_of_stay = sheet[row_num,3]
		amount_of_garbage = sheet[row_num,4]
		recycling_knowledge1 = sheet[row_num,5]
		recycling_knowledge2 = sheet[row_num,6]
		if_they_recycle = sheet[row_num,7]
		if_they_recycle_in_IGC = sheet[row_num,8]
	
		amount_of_garbage_processed = 0
		if amount_of_garbage.to_i == 1
			amount_of_garbage_processed = 1
		elsif amount_of_garbage.to_i == 2
			amount_of_garbage_processed = 2
		elsif amount_of_garbage.to_i == 3
			amount_of_garbage_processed = 3
		end
	
		if_they_recycle_processed = false
		if if_they_recycle == 'Yes'
			if_they_recycle_processed = true
		end
		
		if_they_recycle_in_IGC_processed = false
		if if_they_recycle_in_IGC == 'Yes'
			if_they_recycle_in_IGC_processed = true
		end
	#non_recyclers
		why_no_recycle = sheet[row_num,9]
		improvements = sheet[row_num,14]
		suggestion = sheet[row_num,15]

	#recyclers
		satisfaction = sheet[row_num,12]
		satisfaction_why = sheet[row_num,13]
		improvements = sheet[row_num,14]	
		suggestion = sheet[row_num,15]	
		
		satisfaction_processed = 0
		if satisfaction.to_i == 1
			satisfaction_processed = 1
		elsif satisfaction.to_i == 2
			satisfaction_processed = 2
		elsif satisfaction.to_i == 3
			satisfaction_processed = 3
		elsif satisfaction.to_i == 4
			satisfaction_processed = 4
		elsif satisfaction.to_i == 5
			satisfaction_processed = 5	
		end
		
		recycler= nil
		if if_they_recycle_in_IGC_processed == true
			recycler =	Recycler.create(
				satisfaction: satisfaction_processed,
				satisfaction_why: satisfaction_why,
				improvements: improvements,
				suggestion: suggestion )	
				
		end
		
		non_recycler= nil
		if if_they_recycle_in_IGC_processed == false
		non_recycler = NonRecycler.create(
				why_no_recycle: why_no_recycle,
				improvements: improvements,
				suggestion: suggestion)	
				
		end
		
		participant = Participant.create(
			location: locaton,
			duration_of_stay: duration_of_stay,
			amount_of_garbage: amount_of_garbage_processed,
			recycling_knowledge1: recycling_knowledge1,
			recycling_knowledge2: recycling_knowledge2,
			if_they_recycle: if_they_recycle_processed,
			if_they_recycle_in_IGC: if_they_recycle_in_IGC_processed,
			non_recycler: non_recycler,
			recycler: recycler
			)
		end
  end
end

