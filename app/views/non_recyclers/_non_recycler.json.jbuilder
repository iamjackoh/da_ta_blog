json.extract! non_recycler, :id, :why_no_recycle, :improvements, :suggestion, :participants_id, :created_at, :updated_at
json.url non_recycler_url(non_recycler, format: :json)
