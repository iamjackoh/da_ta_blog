json.extract! recycler, :id, :satisfaction, :satisfaction_why, :improvements, :suggestion, :participants_id, :created_at, :updated_at
json.url recycler_url(recycler, format: :json)
