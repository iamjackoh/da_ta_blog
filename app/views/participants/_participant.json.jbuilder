json.extract! participant, :id, :location, :duration_of_stay, :amount_of_garbage, :recycling_knowledge1, :recycling_knowledge2, :if_they_recycle, :if_they_recycle_in_IGC, :created_at, :updated_at
json.url participant_url(participant, format: :json)
