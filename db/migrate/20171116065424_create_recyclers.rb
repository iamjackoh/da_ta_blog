class CreateRecyclers < ActiveRecord::Migration[5.1]
  def change
    create_table :recyclers do |t|
      t.integer :satisfaction
      t.string :satisfaction_why
      t.string :improvements
      t.string :suggestion
      t.references :participant, foreign_key: true

      t.timestamps
    end
  end
end
