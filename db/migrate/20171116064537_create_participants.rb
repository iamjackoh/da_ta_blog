class CreateParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :participants do |t|
      t.string :location
      t.string :duration_of_stay
      t.integer :amount_of_garbage
      t.string :recycling_knowledge1
      t.string :recycling_knowledge2
      t.boolean :if_they_recycle
      t.boolean :if_they_recycle_in_IGC

      t.timestamps
    end
  end
end
