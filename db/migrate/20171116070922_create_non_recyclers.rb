class CreateNonRecyclers < ActiveRecord::Migration[5.1]
  def change
    create_table :non_recyclers do |t|
      t.string :why_no_recycle
      t.string :improvements
      t.string :suggestion
      t.references :participant, foreign_key: true

      t.timestamps
    end
  end
end
